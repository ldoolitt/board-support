PART = xc5vlx50t-ff1136-1
CLOCK_PIN = K18

ether_dsp_fllrf.ucf: ether_dsp_base.ucf ether_dsp_adc_dac.ucf ether_dsp_dspclk.ucf
	cat $^ > $@

ether_dsp_sim2clk.ucf: ether_dsp_base.ucf ether_dsp_dspclk.ucf
	cat $^ > $@
