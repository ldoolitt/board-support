interface fpga();
wire A10;
wire A12;
wire A13;
wire A14;
wire A15;
wire A17;
wire A18;
wire A19;
wire A20;
wire A22;
wire A23;
wire A24;
wire A25;
wire A27;
wire A28;
wire A29;
wire A30;
wire A32;
wire A36;
wire A37;
wire A9;
wire AA36;
wire AA37;
wire AA41;
wire AA42;
wire AB39;
wire AB40;
wire AC41;
wire AC42;
wire AH18;
wire AH19;
wire AH20;
wire AH21;
wire AH24;
wire AJ14;
wire AJ17;
wire AJ18;
wire AJ21;
wire AJ22;
wire AJ23;
wire AJ24;
wire AJ26;
wire AK14;
wire AK15;
wire AK16;
wire AK17;
wire AK19;
wire AK20;
wire AK21;
wire AK22;
wire AK24;
wire AK25;
wire AK26;
wire AL14;
wire AL15;
wire AL17;
wire AL18;
wire AL19;
wire AL20;
wire AL22;
wire AL23;
wire AL24;
wire AL25;
wire AM13;
wire AM15;
wire AM16;
wire AM17;
wire AM18;
wire AM20;
wire AM21;
wire AM22;
wire AM23;
wire AM25;
wire AM26;
wire AN13;
wire AN14;
wire AN15;
wire AN16;
wire AN18;
wire AN19;
wire AN20;
wire AN21;
wire AN23;
wire AN24;
wire AN25;
wire AN26;
wire AP10;
wire AP11;
wire AP12;
wire AP13;
wire AP14;
wire AP16;
wire AP17;
wire AP18;
wire AP19;
wire AP21;
wire AP22;
wire AP23;
wire AP24;
wire AP26;
wire AR10;
wire AR11;
wire AR12;
wire AR14;
wire AR15;
wire AR16;
wire AR17;
wire AR19;
wire AR20;
wire AR21;
wire AR22;
wire AR24;
wire AR25;
wire AR26;
wire AT10;
wire AT12;
wire AT13;
wire AT14;
wire AT15;
wire AT17;
wire AT18;
wire AT19;
wire AT20;
wire AT22;
wire AT23;
wire AT24;
wire AT25;
wire AU10;
wire AU11;
wire AU12;
wire AU13;
wire AU15;
wire AU16;
wire AU17;
wire AU18;
wire AU20;
wire AU21;
wire AU22;
wire AU23;
wire AU25;
wire AV10;
wire AV11;
wire AV13;
wire AV14;
wire AV15;
wire AV16;
wire AV18;
wire AV19;
wire AV20;
wire AV21;
wire AV23;
wire AV24;
wire AV25;
wire AV9;
wire AW11;
wire AW12;
wire AW13;
wire AW14;
wire AW16;
wire AW17;
wire AW18;
wire AW19;
wire AW21;
wire AW22;
wire AW23;
wire AW24;
wire AW9;
wire AY10;
wire AY11;
wire AY12;
wire AY14;
wire AY15;
wire AY16;
wire AY17;
wire AY19;
wire AY20;
wire AY21;
wire AY22;
wire AY24;
wire AY25;
wire AY9;
wire B10;
wire B11;
wire B12;
wire B13;
wire B15;
wire B16;
wire B17;
wire B18;
wire B20;
wire B21;
wire B22;
wire B23;
wire B25;
wire B26;
wire B27;
wire B28;
wire B30;
wire B31;
wire B32;
wire B39;
wire B40;
wire BA10;
wire BA12;
wire BA13;
wire BA14;
wire BA15;
wire BA17;
wire BA18;
wire BA19;
wire BA20;
wire BA22;
wire BA23;
wire BA24;
wire BA25;
wire BA9;
wire BB10;
wire BB11;
wire BB12;
wire BB13;
wire BB15;
wire BB16;
wire BB17;
wire BB18;
wire BB20;
wire BB21;
wire BB22;
wire BB23;
wire BB25;
wire BB9;
wire C10;
wire C11;
wire C13;
wire C14;
wire C15;
wire C16;
wire C18;
wire C19;
wire C20;
wire C21;
wire C23;
wire C24;
wire C25;
wire C26;
wire C28;
wire C29;
wire C30;
wire C31;
wire C36;
wire C37;
wire C41;
wire C42;
wire C9;
wire D11;
wire D12;
wire D13;
wire D14;
wire D16;
wire D17;
wire D18;
wire D19;
wire D21;
wire D22;
wire D23;
wire D24;
wire D26;
wire D27;
wire D28;
wire D29;
wire D39;
wire D40;
wire D9;
wire E10;
wire E11;
wire E12;
wire E14;
wire E15;
wire E16;
wire E17;
wire E19;
wire E20;
wire E21;
wire E22;
wire E24;
wire E25;
wire E26;
wire E27;
wire E29;
wire E30;
wire E36;
wire E37;
wire E41;
wire E42;
wire E9;
wire F10;
wire F12;
wire F13;
wire F14;
wire F15;
wire F17;
wire F18;
wire F19;
wire F20;
wire F22;
wire F23;
wire F24;
wire F25;
wire F27;
wire F28;
wire F29;
wire F30;
wire F34;
wire F35;
wire F39;
wire F40;
wire F9;
wire G10;
wire G11;
wire G12;
wire G13;
wire G15;
wire G16;
wire G17;
wire G18;
wire G20;
wire G21;
wire G22;
wire G23;
wire G25;
wire G26;
wire G27;
wire G28;
wire G30;
wire G36;
wire G37;
wire G41;
wire G42;
wire H10;
wire H11;
wire H13;
wire H14;
wire H15;
wire H16;
wire H18;
wire H19;
wire H20;
wire H21;
wire H23;
wire H24;
wire H25;
wire H26;
wire H28;
wire H29;
wire H30;
wire H34;
wire H35;
wire H38;
wire H39;
wire H9;
wire J11;
wire J12;
wire J13;
wire J14;
wire J16;
wire J17;
wire J18;
wire J19;
wire J21;
wire J22;
wire J23;
wire J24;
wire J26;
wire J27;
wire J28;
wire J29;
wire J36;
wire J37;
wire J41;
wire J42;
wire K11;
wire K12;
wire K14;
wire K15;
wire K16;
wire K17;
wire K19;
wire K20;
wire K21;
wire K22;
wire K24;
wire K25;
wire K26;
wire K27;
wire K29;
wire K34;
wire K35;
wire K38;
wire K39;
wire L14;
wire L15;
wire L17;
wire L18;
wire L19;
wire L20;
wire L22;
wire L23;
wire L24;
wire L25;
wire L27;
wire L28;
wire L29;
wire L36;
wire L37;
wire L41;
wire L42;
wire M14;
wire M15;
wire M16;
wire M17;
wire M18;
wire M20;
wire M21;
wire M22;
wire M23;
wire M25;
wire M26;
wire M27;
wire M28;
wire M34;
wire M35;
wire M38;
wire M39;
wire N14;
wire N15;
wire N16;
wire N18;
wire N19;
wire N20;
wire N21;
wire N23;
wire N24;
wire N25;
wire N26;
wire N36;
wire N37;
wire N41;
wire N42;
wire P17;
wire P19;
wire P21;
wire P23;
wire P24;
wire P27;
wire P34;
wire P35;
wire P38;
wire P39;
wire R17;
wire R19;
wire R20;
wire R21;
wire R22;
wire R24;
wire R25;
wire R26;
wire R27;
wire R36;
wire R37;
wire R41;
wire R42;
wire T34;
wire T35;
wire T38;
wire T39;
wire U36;
wire U37;
wire U41;
wire U42;
wire V34;
wire V35;
wire V38;
wire V39;
wire W41;
wire W42;
wire Y34;
wire Y35;
wire Y39;
wire Y40;
endinterface