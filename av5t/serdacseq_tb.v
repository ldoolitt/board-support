`timescale 1ns / 1ns
module serdacseq_tb();

reg clk, rst=0;
integer cc;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("serdacseq.vcd");
		$dumpvars(5,serdacseq_tb);
	end
	clk=0;
	#20;
	rst=1;  // active low
	for (cc=0; cc<980; cc=cc+1) begin
		#4; clk=1;
		#4; clk=0;
	end
end

reg clkena=1;
reg ena=0, done_r=0;
reg [31:0] initval=32'h80000001;
reg [11:0] chansel=12'h00f;
reg [15:0] dataval=16'h0000;
always @(posedge clk) begin
	done_r <= done;
	ena <= 1; // (cc==280) | (cc==600);
	if (cc==280) dataval<=16'h1234;
	if (cc==600) dataval<=16'hbeef;
	// if (cc>600) ena <= done_r;
end

wire sdata, sclk, syncn;  // hardware pins
wire done, initdone;  // status outputs
serdacseq mut(
	.clk(clk),
	.clkena(clkena),
	.rst(rst),
	.ena(ena),
	.initval(initval),
	.chansel(chansel),
	.dataval(dataval),
	.done(done),
	.initdone(initdone),
	.sdata(sdata), .sclk(sclk), .syncn(syncn)
);

// Uber-simple chip simulation
reg [31:0] readout, readout_hold;
always @(negedge sclk) if (~syncn) readout <= {readout[30:0],sdata};
always @(posedge syncn) begin
	readout_hold = readout;
	$display("readout %x", readout);
	if (readout == {chansel,dataval}) $display("PASS");
end

endmodule
