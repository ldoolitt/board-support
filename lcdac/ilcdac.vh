wire [7:0] dp_c2m_n;
wire [7:0] dp_c2m_p;
wire [7:0] dp_m2c_n;
wire [7:0] dp_m2c_p;
//wire [7:0] flip=8'b01011010;
wire lmk_sck;
wire dac_sck;
wire lmk_dac_sck;
wire lmk_sdio;
wire dac_sdio;
wire lmk_dac_sdio;
wire dac_sdenb;
wire lmk_cs;
wire dac_sdo;
wire lmk_sdo;
assign lmk_dac_sck = ~lmk_cs ? lmk_sck : (~dac_sdenb ? dac_sck : 1'b0);
assign lmk_dac_sdio = ~lmk_cs ? lmk_sdio : (~dac_sdenb ? dac_sdio : 1'b0); // use 4-wire spi here
wire dac_reset;
wire dac_sync;
wire dac_txen_vadj;
wire fmc_sysref;

wire lmk_clkin0;
wire lmk_dclkout2;
wire lmk_dclkout8;
wire lmk_dclkout10;
wire lmk_sdclkout3;

wire lmk_reset;
wire lmk_sync;

wire prsnt;
wire pg_m2c;
wire pg_c2m;

modport hw(
input dp_c2m_n,dp_c2m_p,lmk_dac_sck,lmk_dac_sdio,dac_sdenb,lmk_cs,dac_reset,dac_txen_vadj,fmc_sysref,lmk_clkin0,lmk_reset,lmk_sync,pg_c2m
,output dp_m2c_p,dp_m2c_n,dac_sdo,lmk_sdo,lmk_dclkout2,lmk_dclkout8,lmk_dclkout10,lmk_sdclkout3,prsnt,pg_m2c,dac_sync
);
modport cfg(
output dp_c2m_n,dp_c2m_p,lmk_sck,dac_sck,lmk_sdio,dac_sdio,dac_sdenb,lmk_cs,dac_reset,dac_txen_vadj,fmc_sysref,lmk_clkin0,lmk_reset,lmk_sync,pg_c2m
,input dp_m2c_p,dp_m2c_n,dac_sdo,lmk_sdo,lmk_dclkout2,lmk_dclkout8,lmk_dclkout10,lmk_sdclkout3,prsnt,pg_m2c,dac_sync
);
modport sim(
output dp_c2m_n,dp_c2m_p,lmk_dac_sck,lmk_sck,dac_sck,lmk_dac_sdio,lmk_sdio,dac_sdio,dac_sdenb,lmk_cs,dac_reset,dac_txen_vadj,fmc_sysref,lmk_clkin0,lmk_reset,lmk_sync,pg_c2m
,input dp_m2c_p,dp_m2c_n,dac_sdo,lmk_sdo,lmk_dclkout2,lmk_dclkout8,lmk_dclkout10,lmk_sdclkout3,prsnt,pg_m2c,dac_sync
);
