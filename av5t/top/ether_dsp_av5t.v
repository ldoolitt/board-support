// Avnet AES-XLX-V5LXT-PCIE50-G
`define SERDES_5T
`define SAME_CLOCKS
`define MY_IP {8'd192, 8'd168, 8'd7, 8'd4}   // 192.168.7.4 non-routable address
`define MY_MAC 48'h00105ad155b3                        // fictitious
`define CAVITY_SIM
`include "ether_fllrf.vh"
