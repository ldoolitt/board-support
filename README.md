# LBL board_support library

## supported carrier boards:
 * `av5t`
 * `cutewr`
 * `llrf4`
 * `llrf46`
 * `lx150t`
 * `ml505`
 * `sp601`
 * `sp605`
 * `spec`

 Each board support package has its base ucf file and config file for key chips
 and interfaces so that the application ucf file could be assembled from
 fpga_family and peripheral_drivers.

## supported daughter boards:
 * `akre_fmc`
 fmc-adc module by Ron Akre and Larry Doolitle

## UCF naming convention:
 $(CARRIER)_$(COMMUNICATION)_fmc.ucf
 where $(COMMUNICATION) could be mgt, gmii or usb.
