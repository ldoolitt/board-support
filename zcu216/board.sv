module board(
	fpga fpga
	,hwif.hw hw
);
//assign hw.vin00_n=fpga.AU4;
//assign hw.vin00_p=fpga.AU5;
//assign hw.vin01_n=fpga.AN4;
//assign hw.vin01_p=fpga.AN5;
//assign hw.vin02_n=fpga.AJ4;
//assign hw.vin02_p=fpga.AJ5;
//assign hw.vin03_n=fpga.AE4;
//assign hw.vin03_p=fpga.AE5;
//assign hw.vin10_n=fpga.AU1;
//assign hw.vin10_p=fpga.AU2;
//assign hw.vin11_n=fpga.AN1;
//assign hw.vin11_p=fpga.AN2;
//assign hw.vin12_n=fpga.AJ1;
//assign hw.vin12_p=fpga.AJ2;
//assign hw.vin13_n=fpga.AE1;
//assign hw.vin13_p=fpga.AE2;
//assign hw.vin20_n=fpga.AR4;
//assign hw.vin20_p=fpga.AR5;
//assign hw.vin21_n=fpga.AL4;
//assign hw.vin21_p=fpga.AL5;
//assign hw.vin22_n=fpga.AG4;
//assign hw.vin22_p=fpga.AG5;
//assign hw.vin23_n=fpga.AC4;
//assign hw.vin23_p=fpga.AC5;
//assign hw.vin30_n=fpga.AR1;
//assign hw.vin30_p=fpga.AR2;
//assign hw.vin31_n=fpga.AL1;
//assign hw.vin31_p=fpga.AL2;
//assign hw.vin32_n=fpga.AG1;
//assign hw.vin32_p=fpga.AG2;
//assign hw.vin33_n=fpga.AC1;
//assign hw.vin33_p=fpga.AC2;
////assign hw.adc0_clk_n=fpga.BB3;
////assign hw.adc0_clk_p=fpga.BA3;
////assign hw.adc1_clk_n=fpga.AY4;
////assign hw.adc1_clk_p=fpga.AW4;
//assign hw.adc2_clk_n=fpga.BB5;
//assign hw.adc2_clk_p=fpga.BA5;
////assign hw.adc3_clk_n=fpga.AY6;
////assign hw.adc3_clk_p=fpga.AW6;
////assign hw.dac0_clk_n=fpga.A3;
////assign hw.dac0_clk_p=fpga.B3;
////assign hw.dac1_clk_n=fpga.C4;
////assign hw.dac1_clk_p=fpga.D4;
//assign hw.dac2_clk_n=fpga.A5;
//assign hw.dac2_clk_p=fpga.B5;
////assign hw.dac3_clk_n=fpga.C6;
////assign hw.dac3_clk_p=fpga.D6;
//assign hw.sysref_in_n=fpga.D1;
//assign hw.sysref_in_p=fpga.D2;
//assign fpga.Y4=hw.vout00_n;
//assign fpga.Y5=hw.vout00_p;
//assign fpga.T4=hw.vout01_n;
//assign fpga.T5=hw.vout01_p;
//assign fpga.M4=hw.vout02_n;
//assign fpga.M5=hw.vout02_p;
//assign fpga.H4=hw.vout03_n;
//assign fpga.H5=hw.vout03_p;
//assign fpga.Y1=hw.vout10_n;
//assign fpga.Y2=hw.vout10_p;
//assign fpga.T1=hw.vout11_n;
//assign fpga.T2=hw.vout11_p;
//assign fpga.M1=hw.vout12_n;
//assign fpga.M2=hw.vout12_p;
//assign fpga.H1=hw.vout13_n;
//assign fpga.H2=hw.vout13_p;
//assign fpga.V4=hw.vout20_n;
//assign fpga.V5=hw.vout20_p;
//assign fpga.P4=hw.vout21_n;
//assign fpga.P5=hw.vout21_p;
//assign fpga.K4=hw.vout22_n;
//assign fpga.K5=hw.vout22_p;
//assign fpga.F4=hw.vout23_n;
//assign fpga.F5=hw.vout23_p;
//assign fpga.V1=hw.vout30_n;
//assign fpga.V2=hw.vout30_p;
//assign fpga.P1=hw.vout31_n;
//assign fpga.P2=hw.vout31_p;
//assign fpga.K1=hw.vout32_n;
//assign fpga.K2=hw.vout32_p;
//assign fpga.F1=hw.vout33_n;
//assign fpga.F2=hw.vout33_p;

wire _clk100;
IBUFGDS ibufgds_clk100(.I(fpga.G12),.IB(fpga.G11),.O(_clk100));
BUFG clk100_bufg(.I(_clk100),.O(hw.clk100));
wire _clk125;
IBUFGDS ibufgds_clk125(.I(fpga.A13),.IB(fpga.A12),.O(_clk125));
BUFG clk125_bufg(.I(_clk125),.O(hw.clk125));


wire _usersi570c0;
IBUFGDS ibufgds_usersi570c0(.I(fpga.AR20),.IB(fpga.AR19),.O(_usersi570c0));
BUFG usersi570c0_bufg(.I(_usersi570c0),.O(hw.usersi570c0));
wire _usersi570c1;
IBUFGDS ibufgds_usersi570c1(.I(fpga.G17),.IB(fpga.F17),.O(_usersi570c1));
BUFG usersi570c1_bufg(.I(_usersi570c1),.O(hw.usersi570c1));

wire _clk104_pl_sysref;
IBUFGDS ibufgds_clk104_pl_sysref(.I(fpga.E11),.IB(fpga.D11),.O(_clk104_pl_sysref));
BUFG clk104_pl_sysref_bufg(.I(_clk104_pl_sysref),.O(hw.clk104_pl_sysref));

wire _clk104_pl_clk;
IBUFGDS ibufgds_clk104_pl_clk(.I(fpga.E10),.IB(fpga.E9),.O(_clk104_pl_clk));
BUFG clk104_pl_clk_bufg(.I(_clk104_pl_clk),.O(hw.clk104_pl_clk));

assign hw.clk104_sync_in=fpga.L15;
assign hw.cpu_reset=fpga.L14;

assign fpga.V38=hw.sfp0_tx_p;
assign fpga.V39=hw.sfp0_tx_n;
//assign fpga.U36=hw.sfp1_tx_p;
//assign fpga.U37=hw.sfp1_tx_n;
assign hw.sfp0_rx_p=fpga.AC41;
assign hw.sfp0_rx_n=fpga.AC42;
//assign hw.sfp1_rx_p=fpga.AB39;
//assign hw.sfp1_rx_n=fpga.AB40;

//wire r8a34001_clk5_in_c;
//ODDRE1 #(.IS_C_INVERTED(1'b0),.IS_D1_INVERTED(1'b0),.IS_D2_INVERTED(1'b0),.SIM_DEVICE("ULTRASCALE_PLUS"),.SRVAL(1'b0)) oddre1_r8a34001_clk5_in_c (.Q(r8a34001_clk5_in_c),.C(hw.clk104_pl_clk),.D1(1'b1),.D2(1'b0),.SR(1'b0));
//OBUFDS_GTE4 obufds_gte4_r8a34001_clk5_in_c(.I(r8a34001_clk5_in_c),.O(fpga.AA36),.OB(fpga.AA37));
//OBUF obuf_r8a34001_clk5_in_c(.I(r8a34001_clk5_in_c),.O(fpga.AA36));
//OBUFDS_GTE4 obufds_gte4_r8a34001_clk5 (.I(hw.clk104_pl_clk),.O(fpga.AA36),.OB(fpga.AA37));
OBUFDS_GTE4 #(.REFCLK_EN_TX_PATH(1'b1),.REFCLK_ICNTL_TX(5'b00111)) obufds_gte4_mgtrefclk0_128 (.O(fpga.AA36),.OB(fpga.AA37),.CEB(1'b0),.I(hw.mgtrefclk0_128));
//wire r8a34001_clk1;
//ODDRE1 #(.IS_C_INVERTED(1'b0),.IS_D1_INVERTED(1'b0),.IS_D2_INVERTED(1'b0),.SIM_DEVICE("ULTRASCALE_PLUS"),.SRVAL(1'b0)) oddre1_r8a34001_clk1 (.Q(r8a34001_clk1),.C(hw.clk104_pl_clk),.D1(1'b1),.D2(1'b0),.SR(1'b0));
//OBUFDS_GTE4 obufds_gte4_r8a34001_clk1 (.I(r8a34001_clk1),.O(fpga.R36),.OB(fpga.R37));
//assign fpga.AA36=hw.r8a34001_clk5_in_c_p;
//assign fpga.AA37=hw.r8a34001_clk5_in_c_n;

IBUFDS_GTE4 #(.REFCLK_EN_TX_PATH(1'b0),.REFCLK_HROW_CK_SEL(2'b00),.REFCLK_ICNTL_RX(2'b00)) ibufds_gte4_mgtrefclk1_128 (.I(fpga.Y34),.IB(fpga.Y35),.CEB(1'b0),.O(hw.mgtrefclk1_128),.ODIV2());
//IBUFGDS ibufgds_gte4_mgtrefclk1_x0y1 (.I(fpga.Y34),.IB(fpga.Y35),.O(hw.mgtrefclk1_x0y1));
//assign hw.r8a34001_q11_out_c_p=fpga.Y34;
//assign hw.r8a34001_q11_out_c_n=fpga.Y35;

assign {fpga.AN14,fpga.C13,fpga.B26}=hw.ledrgb[0];
assign {fpga.AP16,fpga.D14,fpga.E24}=hw.ledrgb[1];
assign {fpga.AP14,fpga.D12,fpga.G26}=hw.ledrgb[2];
assign {fpga.AU16,fpga.D13,fpga.J23}=hw.ledrgb[3];
assign {fpga.AW12,fpga.AW18,fpga.L24}=hw.ledrgb[4];
assign {fpga.AY16,fpga.AV18,fpga.P21}=hw.ledrgb[5];
assign {fpga.BB12,fpga.BA19,fpga.AV21}=hw.ledrgb[6];
assign {fpga.E25,fpga.AP21,fpga.AR21}=hw.ledrgb[7];

assign hw.gpio_sw_s=fpga.H10;
assign hw.gpio_sw_n=fpga.J11;
assign hw.gpio_sw_e=fpga.J12;
assign hw.gpio_sw_c=fpga.K11;
assign hw.gpio_sw_w=fpga.K12;
assign hw.gpio_dip_sw0=fpga.AY10;
assign hw.gpio_dip_sw1=fpga.AY11;
assign hw.gpio_dip_sw2=fpga.BA9;
assign hw.gpio_dip_sw3=fpga.AY9;
assign hw.gpio_dip_sw4=fpga.BB9;
assign hw.gpio_dip_sw5=fpga.BA10;
assign hw.gpio_dip_sw6=fpga.BB10;
assign hw.gpio_dip_sw7=fpga.BB11;

viabus #(.WIDTH(7)) pmod0via (.a({fpga.J14,fpga.J13,fpga.H13,fpga.G13,fpga.H15,fpga.H14,fpga.G16}),.b(hw.pmod0[7:1]));
viabus #(.WIDTH(8)) pmod1via (.a({fpga.N16,fpga.M16,fpga.N15,fpga.M15,fpga.N14,fpga.M14,fpga.M17,fpga.L17}),.b(hw.pmod1));
viabus #(.WIDTH(16)) daciovia (.a({fpga.B13,fpga.B12,fpga.F15,fpga.E14,fpga.C15,fpga.C14,fpga.B16,fpga.B15,fpga.E16,fpga.E15,fpga.D16,fpga.C16,fpga.A15,fpga.A14,fpga.F14,fpga.F13}),.b(hw.dacio));
viabus #(.WIDTH(16)) adciovia (.a({fpga.AV11,fpga.AW11,fpga.AV9,fpga.AW9,fpga.AU10,fpga.AV10,fpga.AU12,fpga.AU11,fpga.AR12,fpga.AT12,fpga.AR10,fpga.AT10,fpga.AP12,fpga.AR11,fpga.AP11,fpga.AP10}),.b(hw.adcio));

//assign {fpga.B10,fpga.A10,fpga.G10,fpga.H11,fpga.AT12,fpga.AR12,fpga.AV11,fpga.AR10}=hw.leds;
//assign hw.buttons={fpga.AU10,fpga.BB22,fpga.BB23,fpga.BB25,fpga.BA25};

endmodule

module board_sim(
	fpga fpga
	,hwif.sim hw
);

assign fpga.G12=hw.clk100;
assign fpga.G11=~hw.clk100;
assign fpga.A13=hw.clk125;
assign fpga.A12=~hw.clk125;
assign fpga.AR20=hw.usersi570c0;
assign fpga.AR19=~hw.usersi570c0;
assign fpga.G17=hw.usersi570c1;
assign fpga.F17=~hw.usersi570c1;
assign fpga.E11=hw.clk104_pl_sysref;
assign fpga.D11=~hw.clk104_pl_sysref;
assign fpga.E10=hw.clk104_pl_clk;
assign fpga.E9=~hw.clk104_pl_clk;

assign fpga.L15=hw.clk104_sync_in;
assign fpga.L14=hw.cpu_reset;

endmodule
