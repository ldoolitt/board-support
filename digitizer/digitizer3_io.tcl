proc digitizer3_io { } {
	dict set pinio modulename digitizer3
set inlist { P1.C10 P1.C11 P1.C14 P1.C15 P1.C18 P1.C19 P1.C22 P1.C23 P1.D11 P1.D12 P1.D14 P1.D15 P1.D20 P1.D21 P1.D8 P1.D9 P1.G10 P1.G12 P1.G13 P1.G18 P1.G19 P1.G21 P1.G22 P1.G24 P1.G25 P1.G6 P1.G7 P1.G9 P1.H10 P1.H11 P1.H13 P1.H14 P1.H16 P1.H17 P1.H19 P1.H20 P1.H22 P1.H23 P1.H25 P1.H26 P1.H7 P1.H8 P2.D20 P2.D21 P2.D27 P2.H22 P2.H28 P2.H4 P2.H5 }
set outlist { P2.C10 P2.C11 P2.C14 P2.C15 P2.C18 P2.C19 P2.C26 P2.C27 P2.D11 P2.D12 P2.D14 P2.D15 P2.D17 P2.D18 P2.D26 P2.G10 P2.G12 P2.G13 P2.G15 P2.G16 P2.G18 P2.G18 P2.G19 P2.G24 P2.G25 P2.G30 P2.G31 P2.G33 P2.G34 P2.G36 P2.G36 P2.G37 P2.G6 P2.G7 P2.G9 P2.H10 P2.H11 P2.H13 P2.H14 P2.H16 P2.H17 P2.H19 P2.H20 P2.H23 P2.H25 P2.H26 P2.H29 P2.H32 P2.H7 P2.H8 }
dict set pinio inlist $inlist
dict set pinio outlist $outlist
return $pinio
}
if { ![info exists pinioproc] } {
	global pinioproc 
	set pinioproc {}
}
puts "running pinioproc"
puts $pinioproc
lappend pinioproc digitizer3_io
