module rearpanelio_tb(inout [7:0] PMOD
,inout [3:0] db9
,output J6U6
,inout J3
,input J4
,input J5
);
// pmod pin assignment mismatch between the rearpanel io board and digitizer
//rearpanlio             digitizer board   But the name was using standard 12pin PMOD in firmware,
//   1 2                    1 8                    1 7
//   3 4                    2 9                    2 8
//   5 6                    3 10                   3 9
//   7 8                    4 11                   4 10
//   9 10                   5 12                   5 11
//  11 12                   6 13                   6 12
//  13 14                   7 14
//

//      on schematic rearpanel_20170521.pdf 1234a987
// db9 port 4 pins
//  0 RXD  FPGA input
//  1 TXD  FPGA output
//  2 DTR  FPGA output
//  3 DSR  FPGA input

assign PMOD[0]=db9[0]; //J1_2  J2_1             1
assign PMOD[1]=db9[3]; //J1_6 J2_3             2
assign J6U6=PMOD[2]; // J2_5             3
//assign PMOD[3]=J3; // J2_7               4
via j3via(J3,PMOD[3]);
assign db9[2]=PMOD[4]; //J1_4 J2_2             7
assign db9[1]=PMOD[5]; //J1_3 J2_4             8
assign PMOD[6]=J5; // J2_6               9
assign PMOD[7]=J4; // J2_8               a
endmodule
