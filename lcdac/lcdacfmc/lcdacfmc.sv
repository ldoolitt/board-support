`timescale 1ns / 100ps

module lcdacfmc(ifmchpcpin.pin fmcpin,ilcdac.hw lcdacfmc,con14 J1,con12 J2);
lcdacfmc_v lcdacfmc_v(
`include "fmcpin_vinst.vh"
,.lcdacfmc(lcdacfmc)
,.J1(J1),.J2(J2)
);
endmodule

module lcdacfmc_v(
`include "fmcpin_vport.vh"
,ilcdac.hw lcdacfmc
,con14 J1
,con12 J2
);
ifmchpcpin fmcpin();
`include "fmcpin_via.vh"
lcdacfmc_i lcdacfmc_i(.fmcpin(fmcpin.pin),.lcdacfmc(lcdacfmc),.J1(J1),.J2(J2));
endmodule

module lcdacfmc_i(ifmchpcpin.pin fmcpin,ilcdac.hw lcdacfmc,con14 J1,con12 J2);
`include "lcdacfmc.vh"
endmodule

//module lcdacfmc_2(ifmchpcpin_2.pin fmcpin,ilcdac_2.hw lcdacfmc);
//`include "lcdacfmc.vh"
//endmodule
